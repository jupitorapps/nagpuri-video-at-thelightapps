package com.thelightapps.nagpurivideo;

import android.view.View;

/**
 * Created by Ravi on 26-Mar-17.
 */

public interface ClickListener {

    void onClick(View view, int position);

    void onLongClick(View view, int position);
}
