package com.thelightapps.nagpurivideo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by FIRST on 24-08-2017.
 */

public class AutoCompleteAdapter extends ArrayAdapter<VideoGeterSeter> {
    private ArrayList<VideoGeterSeter> items;
    //private ArrayList<VideoGeterSeter> itemsAll;
    //private ArrayList<VideoGeterSeter> suggestions;
    private int viewResourceId;

    public AutoCompleteAdapter(Context context, int viewResourceId, ArrayList<VideoGeterSeter> items) {
        super(context, viewResourceId, items);
        this.items = (ArrayList<VideoGeterSeter>) items.clone();
        this.viewResourceId = viewResourceId;
        //this.itemsAll = (ArrayList<VideoGeterSeter>) items.clone();
        //this.suggestions = new ArrayList<VideoGeterSeter>();
        this.viewResourceId = viewResourceId;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(viewResourceId, null);
        }
        VideoGeterSeter string = items.get(position);
        if (string != null) {
            TextView customerNameLabel = (TextView) v.findViewById(R.id.autotext);
            if (customerNameLabel != null) {
                customerNameLabel.setText(string.getName());
            }
        }
        return v;
    }
    /*@Override
    public Filter getFilter() {
        return nameFilter;
    }

    Filter nameFilter = new Filter() {
        @Override
        public String convertResultToString(Object resultValue) {
            String str = ((VideoGeterSeter)(resultValue)).getName();
            return str;
        }
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if(constraint != null) {
                suggestions.clear();
                for (VideoGeterSeter videoGeterSeter : itemsAll) {
                    if(videoGeterSeter.getName().toLowerCase().startsWith(constraint.toString().toLowerCase())){
                        suggestions.add(videoGeterSeter);
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            ArrayList<VideoGeterSeter> filteredList = (ArrayList<VideoGeterSeter>) results.values;
            if(results != null && results.count > 0) {
                clear();
                for (VideoGeterSeter v : filteredList) {
                    add(v);
                }
                notifyDataSetChanged();
            }
        }
    };*/
}