package com.thelightapps.nagpurivideo;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Telephony;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

public class VideoDetail extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener  {

    ColorFilter cf;
    RelativeLayout relHeader,relBottom;
    TextView txtHeader;
    Button btnShare;
    String myTheme,strRegisterId,StatusImageLikeorNot,strVideo,strCode,strName;
    RelativeLayout relScreen;
    Button btnBack;
    ImageView btnFavorite,btnDownload;
    String strId,isFav;
    ArrayList<VideoGeterSeter> arrVideoLis;
    int myPosition;
    View layout12;
    Typeface tf;
    private YouTubePlayerView youTubeView;
    // String developer_key = getString(R.string.youTubeAPIKey);
    private static final int RECOVERY_DIALOG_REQUEST = 1;
    private MyPlaybackEventListener playbackEventListener;
//    private MyPlayerStateChangeListener playerStateChangeListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        tf = Typeface.createFromAsset(VideoDetail.this.getAssets(),"fonts/Raleway-Light.ttf");

        SharedPreferences prefs = getSharedPreferences("MY_PREFS_NAME", Context.MODE_PRIVATE);
        myTheme = prefs.getString("Color", null);
        strRegisterId = prefs.getString("RegisterId",null);
        Log.d("myRegisterIdDetail",""+strRegisterId);

        setContentView(R.layout.activity_video_detail);

        relHeader = (RelativeLayout) findViewById(R.id.relHeader);
        relBottom = (RelativeLayout) findViewById(R.id.relBottom);

        if (myTheme!=null){

            relHeader.setBackgroundColor(Color.parseColor(myTheme));

        } else {

        }

        btnShare = (Button) findViewById(R.id.btnShare);

        txtHeader = (TextView) findViewById(R.id.txtHeader);

        txtHeader.setTypeface(tf);

        btnBack = (Button) findViewById(R.id.btnBack);

        Intent iv = getIntent();
        strId = iv.getStringExtra("Id");
        Log.d("DtId",""+strId);
        strVideo = iv.getStringExtra("Video");
        Log.d("strVideo",""+strVideo);
        strCode = iv.getStringExtra("Code");
        Log.d("Code",""+strCode);
        String ArraySize = iv.getStringExtra("ArraySize");
        Log.d("ArraySize",""+ArraySize);
        String Position = iv.getStringExtra("Position");
        Log.d("Position",""+Position);
        strName = iv.getStringExtra("Name");
        Log.d("Name",""+strName);

        try {
            myPosition = Integer.parseInt(Position);
        } catch(NumberFormatException nfe) {
            System.out.println("Could not parse " + nfe);
        }

        txtHeader.setText(strName);

        youTubeView = (YouTubePlayerView) findViewById(R.id.youtube_view);
        youTubeView.initialize(getResources().getString(R.string.youTubeAPIKey), this);

        playbackEventListener = new MyPlaybackEventListener();
        //playerStateChangeListener = new MyPlayerStateChangeListener();

        btnFavorite = (ImageView) findViewById(R.id.btnFavourite);

        btnFavorite.setImageResource(R.drawable.like);
        btnFavorite.setColorFilter(Util.changeImageColor(btnFavorite));

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        new getImageLikeOrNot().execute();

        new getMostViewImage().execute();

        btnFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                Log.d("Button",""+StatusImageLikeorNot);

                if (StatusImageLikeorNot!=null){

                    if (StatusImageLikeorNot.equals("True")){


                    } else if (StatusImageLikeorNot.equals("False")){

                        if (myTheme!=null) {

                            cf = new PorterDuffColorFilter(Color.parseColor(myTheme), PorterDuff.Mode.SRC_IN);
                            btnFavorite.setColorFilter(cf);

                        }else {
                            cf = new PorterDuffColorFilter(Color.parseColor("#000000"), PorterDuff.Mode.SRC_IN);
                            btnFavorite.setColorFilter(cf);
                        }

                        new LikeUpdate().execute();

                    }

                } else {

                    if (myTheme!=null) {

                        cf = new PorterDuffColorFilter(Color.parseColor(myTheme), PorterDuff.Mode.SRC_IN);
                        btnFavorite.setColorFilter(cf);

                    }else {
                        cf = new PorterDuffColorFilter(Color.parseColor("#000000"), PorterDuff.Mode.SRC_IN);
                        btnFavorite.setColorFilter(cf);
                    }

                    new LikeUpdate().execute();

                }

            }
        });

        btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //new getMostShare().execute();

                final RelativeLayout rl_dialoguser = (RelativeLayout) findViewById(R.id.rl_infodialog);
                rl_dialoguser.setVisibility(View.VISIBLE);
                layout12 = getLayoutInflater().inflate(R.layout.sharefriend, rl_dialoguser, false);

                rl_dialoguser.addView(layout12);

                Button btnFacebook = (Button) layout12.findViewById(R.id.btnFacebook);
                Button btnEmail = (Button) layout12.findViewById(R.id.btnEmail);
                Button btnMessage = (Button) layout12.findViewById(R.id.btnMessage);
                Button btnTwitter = (Button) layout12.findViewById(R.id.btnTwitter);
                Button btnWhatsapp = (Button) layout12.findViewById(R.id.btnWhatsapp);
                Button btnExit = (Button) layout12.findViewById(R.id.btnExit);

                btnFacebook.setTransformationMethod(null);
                btnEmail.setTransformationMethod(null);
                btnMessage.setTransformationMethod(null);
                btnTwitter.setTransformationMethod(null);
                btnWhatsapp.setTransformationMethod(null);
                btnExit.setTransformationMethod(null);

                GradientDrawable gdShare = new GradientDrawable();
                GradientDrawable gdExit = new GradientDrawable();

                if (myTheme!=null) {

                    gdShare.setShape(GradientDrawable.RECTANGLE);
                    gdShare.setColor(Color.parseColor(myTheme));
                    gdShare.setStroke(1, Color.WHITE);
                    gdShare.setCornerRadius(1.0f);

                    gdExit.setShape(GradientDrawable.RECTANGLE);
                    gdExit.setColor(Color.WHITE);
                    gdExit.setStroke(1, Color.parseColor(myTheme));
                    gdExit.setCornerRadius(5.0f);

                    btnExit.setTextColor(Color.parseColor(myTheme));

                }else {

                    gdShare.setShape(GradientDrawable.RECTANGLE);
                    gdShare.setColor(Color.parseColor("#f7412c"));
                    gdShare.setStroke(1, Color.WHITE);
                    gdShare.setCornerRadius(1.0f);

                    gdExit.setShape(GradientDrawable.RECTANGLE);
                    gdExit.setColor(Color.WHITE);
                    gdExit.setStroke(1, Color.parseColor("#f7412c"));
                    gdExit.setCornerRadius(5.0f);

                }

                btnFacebook.setBackground(gdShare);
                btnEmail.setBackground(gdShare);
                btnMessage.setBackground(gdShare);
                btnTwitter.setBackground(gdShare);
                btnWhatsapp.setBackground(gdShare);

                btnExit.setBackground(gdExit);

                btnFacebook.setTypeface(tf);
                btnEmail.setTypeface(tf);
                btnMessage.setTypeface(tf);
                btnTwitter.setTypeface(tf);
                btnWhatsapp.setTypeface(tf);
                btnExit.setTypeface(tf);

                btnExit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        View myView = findViewById(R.id.rl_back);
                        ViewGroup parent = (ViewGroup) myView.getParent();
                        parent.removeView(myView);

                    }
                });

                btnFacebook.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Intent intent = new Intent(Intent.ACTION_SEND);
                        intent.setType("text/plain");
                        intent.putExtra(Intent.EXTRA_TEXT, strVideo);
                        // See if official Facebook app is found
                        boolean facebookAppFound = false;
                        List<ResolveInfo> matches = getPackageManager().queryIntentActivities(intent, 0);
                        for (ResolveInfo info : matches) {
                            if (info.activityInfo.packageName.toLowerCase().startsWith("com.facebook")) {
                                intent.setPackage(info.activityInfo.packageName);
                                facebookAppFound = true;
                                break;
                            }
                        }
                        // As fallback, launch sharer.php in a browser
                        if (!facebookAppFound) {
                            String sharerUrl = getResources().getString(R.string.facebookSharerUrl) + strVideo;
                            intent = new Intent(Intent.ACTION_VIEW, Uri.parse(sharerUrl));
                        }
                        startActivity(intent);

                        View myView = findViewById(R.id.rl_back);
                        ViewGroup parent = (ViewGroup) myView.getParent();
                        parent.removeView(myView);

                    }
                });

                btnEmail.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        try {

                            Intent gmail = new Intent(Intent.ACTION_VIEW);
                            gmail.setClassName("com.google.android.gm", "com.google.android.gm.ComposeActivityGmail");
                            gmail.putExtra(Intent.EXTRA_EMAIL, new String[] { "" });
                            gmail.setData(Uri.parse(""));
                            gmail.putExtra(Intent.EXTRA_SUBJECT, "Best App for Odia Video Songs:");
                            gmail.setType("text/plain");
                            gmail.setType("image/jpeg");
                            gmail.putExtra(Intent.EXTRA_TEXT, strVideo);
                            startActivity(gmail);

                        } catch (Exception e) {

                            sendEmail();

                        }

                        //rl_dialoguser.setVisibility(View.GONE);
                        View myView = findViewById(R.id.rl_back);
                        ViewGroup parent = (ViewGroup) myView.getParent();
                        parent.removeView(myView);

                    }
                });

                btnTwitter.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        try {

                            ImageView imageView1 = new ImageView(VideoDetail.this);
                            imageView1.setImageResource(R.drawable.ic_launcher);
                            imageView1.setVisibility(View.GONE);

//
                            Intent intent = new Intent();
                            intent.setAction(Intent.ACTION_SEND);
                            intent.putExtra(Intent.EXTRA_TEXT, strVideo);
                            intent.setType("text/plain");
                            intent.setPackage("com.twitter.android");
                            startActivity(intent);
                        } catch (ActivityNotFoundException e) {
//
                            Toast.makeText(VideoDetail.this, "Twitter not install", Toast.LENGTH_LONG).show();
                        }

                        //rl_dialoguser.setVisibility(View.GONE);
                        View myView = findViewById(R.id.rl_back);
                        ViewGroup parent = (ViewGroup) myView.getParent();
                        parent.removeView(myView);

                    }
                });

                btnMessage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        //Share Image and Text

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) // At least KitKat
                        {
                            String defaultSmsPackageName = Telephony.Sms.getDefaultSmsPackage(getApplication()); // Need to change the build to API 19

                            Intent sendIntent = new Intent(Intent.ACTION_SEND);
                            sendIntent.setType("vnd.android-dir/mms-sms");
                            sendIntent.putExtra("sms_body", strVideo);
                            //sendIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(MediaStore.Images.Media.insertImage(getContentResolver(), BitmapFactory.decodeResource(getResources(),R.drawable.ic_launcher),"title", null)));
                            sendIntent.setType("image/png");

                            if (defaultSmsPackageName != null)// Can be null in case that there is no default, then the user would be able to choose
                            // any app that support this intent.
                            {
                                sendIntent.setPackage(defaultSmsPackageName);
                            }
                            startActivity(sendIntent);

                        }
                        else
                        {
                            Intent smsIntent = new Intent(android.content.Intent.ACTION_VIEW);
                            smsIntent.setType("vnd.android-dir/mms-sms");
                            smsIntent.putExtra("sms_body", strVideo);
                            //smsIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(MediaStore.Images.Media.insertImage(getContentResolver(), BitmapFactory.decodeResource(getResources(),R.drawable.ic_launcher),"title", null)));
                            smsIntent.setType("image/png");
                            startActivity(smsIntent);
                        }

                        View myView = findViewById(R.id.rl_back);
                        ViewGroup parent = (ViewGroup) myView.getParent();
                        parent.removeView(myView);

                    }
                });

                btnWhatsapp.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        try {
                            String urlToShare = strVideo;

                            Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
                            whatsappIntent.putExtra(Intent.EXTRA_TEXT, urlToShare);
                            whatsappIntent.setType("text/plain");
                            whatsappIntent.setPackage("com.whatsapp");
                            startActivity(whatsappIntent);
                            // Detailpage.this.startActivity(whatsappIntent);

                        } catch (ActivityNotFoundException ex) {
                            Toast.makeText(VideoDetail.this, "Whatsapp have not been installed.", Toast.LENGTH_LONG)
                                    .show();
                        }

                        View myView = findViewById(R.id.rl_back);
                        ViewGroup parent = (ViewGroup) myView.getParent();
                        parent.removeView(myView);

                    }
                });

            }
        });

    }

    private void sendEmail() {
        // TODO Auto-generated method stub
        String recipient = "";
        String subject = "Best App for Odia Video Songs:";
        @SuppressWarnings("unused")
        String body = "";

        String[] recipients = { recipient };
        Intent email = new Intent(Intent.ACTION_SEND);

        email.setType("message/rfc822");

        email.putExtra(Intent.EXTRA_EMAIL, strVideo);
        email.putExtra(Intent.EXTRA_SUBJECT, subject);

        try {

            startActivity(Intent.createChooser(email, ""));

        } catch (android.content.ActivityNotFoundException ex) {

            //Toast.makeText(Setting.this, getString(R.string.email_no_client), Toast.LENGTH_LONG).show();

        }
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
        if (!b) {
            youTubePlayer.setPlaybackEventListener(playbackEventListener);
            //youTubePlayer.setPlayerStateChangeListener(playerStateChangeListener);
            // loadVideo() will auto play video
            // Use cueVideo() method, if you don't want to play it automatically
            /*Log.d("videocode",""+strCode);
            youTubePlayer.cueVideo(strCode);
            // Hiding player controls
            youTubePlayer.setPlayerStyle(YouTubePlayer.PlayerStyle.DEFAULT);*/

            youTubePlayer.cueVideo(strCode); // Plays https://www.youtube.com/watch?v=fhWaJi1Hsfo


        }
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
        if (youTubeInitializationResult.isUserRecoverableError()) {
            youTubeInitializationResult.getErrorDialog(this, RECOVERY_DIALOG_REQUEST).show();
        } else {
            String errorMessage = String.format(
                    "There was an error initializing youtube player", youTubeInitializationResult.toString());
            Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RECOVERY_DIALOG_REQUEST) {
            // Retry initialization if user performed a recovery action
            getYouTubePlayerProvider().initialize(getResources().getString(R.string.youTubeAPIKey), this);
        }
    }

    private YouTubePlayer.Provider getYouTubePlayerProvider() {
        return (YouTubePlayerView) findViewById(R.id.youtube_view);
    }

    private final class MyPlaybackEventListener implements YouTubePlayer.PlaybackEventListener {

        @Override
        public void onPlaying() {
            // Called when playback starts, either due to user action or call to play().
            //showMessage("Playing");

            relHeader.setVisibility(View.GONE);

        }

        @Override
        public void onPaused() {
            // Called when playback is paused, either due to user action or call to pause().
            //showMessage("Paused");

            relHeader.setVisibility(View.VISIBLE);

        }

        @Override
        public void onStopped() {
            // Called when playback stops for a reason other than being paused.
            //showMessage("Stopped");
        }

        @Override
        public void onBuffering(boolean b) {
            // Called when buffering starts or ends.
        }

        @Override
        public void onSeekTo(int i) {
            // Called when a jump in playback position occurs, either
            // due to user scrubbing or call to seekRelativeMillis() or seekToMillis()
        }
    }

    private void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    /*private final class MyPlayerStateChangeListener implements YouTubePlayer.PlayerStateChangeListener {

        @Override
        public void onLoading() {
            // Called when the player is loading a video
            // At this point, it's not ready to accept commands affecting playback such as play() or pause()
        }

        @Override
        public void onLoaded(String s) {
            // Called when a video is done loading.
            // Playback methods such as play(), pause() or seekToMillis(int) may be called after this callback.
        }

        @Override
        public void onAdStarted() {
            // Called when playback of an advertisement starts.
        }

        @Override
        public void onVideoStarted() {
            // Called when playback of the video starts.
        }

        @Override
        public void onVideoEnded() {
            // Called when the video reaches its end.
        }

        @Override
        public void onError(YouTubePlayer.ErrorReason errorReason) {
            // Called when an error occurs.
        }
    }*/

    public class getMostViewImage extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... params) {
            getImageviewMe();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

        }
    }

    private void getImageviewMe() {
        //arrVideoLis.clear();
        URL hp = null;
        try {

            hp = new URL(getString(R.string.COMMON_URL)+getResources().getString(R.string.videoViewUrl)+strId+"&user_id="+strRegisterId);

            Log.d("hp",""+hp);

            URLConnection hpCon = hp.openConnection();
            hpCon.connect();
            InputStream input = hpCon.getInputStream();
            BufferedReader r = new BufferedReader(new InputStreamReader(input));
            String x = "";
            x = r.readLine();
            String total = "";

            while (x != null) {
                total += x;
                x = r.readLine();
            }
            JSONObject jObject = new JSONObject(total);
            Log.d("Object",""+jObject);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
        }
    }

    public class getImageLikeOrNot extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            /*progressDialog = new ProgressDialog(Detailpage.this);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(true);
            progressDialog.show();*/
        }

        @Override
        protected Void doInBackground(Void... params) {
            getImageLike();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            /*if (progressDialog.isShowing()) {
                progressDialog.dismiss();*/
            Log.d("Button1",""+StatusImageLikeorNot);
            if (StatusImageLikeorNot!=null){

                if (StatusImageLikeorNot.equals("True")){

                    if (myTheme!=null) {

                        cf = new PorterDuffColorFilter(Color.parseColor(myTheme), PorterDuff.Mode.SRC_IN);
                        btnFavorite.setColorFilter(cf);

                    }else {
                        cf = new PorterDuffColorFilter(Color.parseColor("#000000"), PorterDuff.Mode.SRC_IN);
                        btnFavorite.setColorFilter(cf);
                    }


                } else if (StatusImageLikeorNot.equals("False")){

                    btnFavorite.setImageResource(R.drawable.like);
                    btnFavorite.setColorFilter(Util.changeImageColor(btnFavorite));

                }
            } else {

            }

            //}
        }
    }

    private void getImageLike() {
        //arrImageList.clear();
        URL hp = null;
        try {

            hp = new URL(getString(R.string.COMMON_URL)+getResources().getString(R.string.checklickVideoUrl)+strId+"&user_id="+strRegisterId);

            Log.d("hp",""+hp);

            URLConnection hpCon = hp.openConnection();
            hpCon.connect();
            InputStream input = hpCon.getInputStream();
            BufferedReader r = new BufferedReader(new InputStreamReader(input));
            String x = "";
            x = r.readLine();
            String total = "";

            while (x != null) {
                total += x;
                x = r.readLine();
            }
            JSONObject jObject = new JSONObject(total);
            Log.d("Object",""+jObject);

            StatusImageLikeorNot = jObject.getString("status");
            Log.d("Status",""+StatusImageLikeorNot);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
        }
    }

    public class LikeUpdate extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... params) {
            getLikeUpdate();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

        }

    }

    private void getLikeUpdate() {
        // TODO Auto-generated method stub

        URL hp = null;
        try {

            hp = new URL(getString(R.string.COMMON_URL)+getResources().getString(R.string.videoLikeUrl)+strId+"&user_id="+strRegisterId);
            Log.d("URL", "" + hp);
            URLConnection hpCon = hp.openConnection();
            hpCon.connect();
            InputStream input = hpCon.getInputStream();
            Log.d("input", "" + input);

            BufferedReader r = new BufferedReader(new InputStreamReader(input));

            String x = "";
            x = r.readLine();
            String total = "";

            while (x != null) {
                total += x;
                x = r.readLine();
            }
            Log.d("URL", "" + total);

            JSONObject jObject = new JSONObject(total);
            Log.d("Object",""+jObject);
            JSONObject newJObject = jObject.getJSONObject("GalleryDetail");

            UserGeterSeter temp = new UserGeterSeter();

            temp.setId(newJObject.getString("id"));

            Log.d("LikeID",""+newJObject.getString("id"));

        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (NullPointerException e) {
            // TODO: handle exception
        }
    }

}
