package com.thelightapps.nagpurivideo;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Redixbit on 22-09-2016.
 */
public class VideoGeterSeter extends RealmObject {

    @PrimaryKey
    private String id;
    private String video;
    private String ytcode;
    private String name;
    private String description;
    private String cat_id;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getYtcode() {
        return ytcode;
    }

    public void setYtcode(String ytcode) {
        this.ytcode = ytcode;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCat_id() {
        return cat_id;
    }

    public void setCat_id(String cat_id) {
        this.cat_id = cat_id;
    }

}
