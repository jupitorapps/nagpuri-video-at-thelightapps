package com.thelightapps.nagpurivideo;

import android.app.Activity;
import android.app.Application;
import android.support.v4.app.Fragment;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

/**
 * Created by FIRST on 25-08-2017.
 */

public class RealmController
{
    private static RealmController instance;
    private final Realm realm;

    public RealmController(Application application) {
        realm = Realm.getDefaultInstance();
    }

    public static RealmController with(Fragment fragment) {

        if (instance == null) {
            instance = new RealmController(fragment.getActivity().getApplication());
        }
        return instance;
    }

    public static RealmController with(Activity activity) {

        if (instance == null) {
            instance = new RealmController(activity.getApplication());
        }
        return instance;
    }

    public static RealmController with(Application application) {

        if (instance == null) {
            instance = new RealmController(application);
        }
        return instance;
    }

    public static RealmController getInstance() {

        return instance;
    }

    public Realm getRealm() {
        return realm;
    }

    //Refresh the realm istance
    public void refresh() {
        realm.refresh();
    }

    //clear all objects from Book.class
    public void clearAllVideos() {
        realm.beginTransaction();
        realm.clear(VideoGeterSeter.class);
        realm.commitTransaction();
    }

    public void add(String id,String name,String videos, String ytcode, String description,String cat_id){
        realm.beginTransaction();
        VideoGeterSeter video = realm.createObject(VideoGeterSeter.class);
        video.setId(id);
        video.setName(name);
        video.setVideo(videos);
        video.setYtcode(ytcode);
        video.setDescription(description);
        video.setCat_id(cat_id);
        realm.commitTransaction();
    }

    public  void deleteById(final String id)
    {

       /* realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<VideoGeterSeter> rows = realm.where(VideoGeterSeter.class).equalTo("id",id).findAll();
                rows.clear();
            }
        });*/
        RealmResults<VideoGeterSeter> results = realm.where(VideoGeterSeter.class).findAll();
        VideoGeterSeter v = results.where().equalTo("id",id).findFirst();
        if (v != null){
            realm.beginTransaction();
            v.removeFromRealm();
            realm.commitTransaction();
        }

    }

    public void delelteVideo(final String id)
    {
        realm.beginTransaction();
        RealmQuery query = realm.where(VideoGeterSeter.class);
        RealmResults results = query.findAll();
        results.remove(id);
        realm.commitTransaction();
    }

    //find all objects in the Book.class
    public RealmResults<VideoGeterSeter> getVideos() {

        return realm.where(VideoGeterSeter.class).findAll();
    }

    //query a single item with the given id
    public VideoGeterSeter getVideoByid(String id) {
        return realm.where(VideoGeterSeter.class).equalTo("id", id).findFirst();
    }



    //check if Book.class is empty
    public boolean hasVideos() {
        return !realm.allObjects(VideoGeterSeter.class).isEmpty();
    }

    //query example
    public RealmResults<VideoGeterSeter> queryedVideos() {

        return realm.where(VideoGeterSeter.class).findAll();

    }
}
