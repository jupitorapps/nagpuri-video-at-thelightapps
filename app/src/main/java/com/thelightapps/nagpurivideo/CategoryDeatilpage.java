package com.thelightapps.nagpurivideo;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.InterstitialAd;
import com.facebook.ads.InterstitialAdListener;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;


public class CategoryDeatilpage extends Activity {

    String LOG_TAG = this.getClass().getSimpleName();
    RelativeLayout relList,relNodata;
    Button btnBack;
    ProgressDialog progressDialog;
    GridView Categoriesgrid;
    ArrayList<VideoGeterSeter> arrVideoList;
    String strId,myTheme,strTitle;
    boolean interstitialCanceled;
    private ConnectionDetector cd;
    int MAINPOSITION ;
    String adVisibility;
    SharedPreferences prefs;
    Typeface tf;
    private InterstitialAd interstitialAd;
    private AdmobInterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        tf = Typeface.createFromAsset(CategoryDeatilpage.this.getAssets(),"fonts/Raleway-Light.ttf");

        prefs = CategoryDeatilpage.this.getSharedPreferences("MY_PREFS_NAME", Context.MODE_PRIVATE);
        myTheme = prefs.getString("Color", null);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_category_deatilpage);

        relList = (RelativeLayout) findViewById(R.id.relList);
        relNodata = (RelativeLayout) findViewById(R.id.relNodata);

        // Instantiate an InterstitialAd object
        interstitialAd = new InterstitialAd(this, getResources().getString(R.string.placement_id_fb_interstial));

        RelativeLayout relHeader = (RelativeLayout) findViewById(R.id.relHeader);

        if (myTheme!=null){

            relHeader.setBackgroundColor(Color.parseColor(myTheme));

        } else {

        }

        btnBack = (Button) findViewById(R.id.btnBack);

        Categoriesgrid = (GridView) findViewById(R.id.Categoriesgrid) ;

        Intent iv = getIntent();
        strId = iv.getStringExtra("Id");
        Log.d("cdId",""+strId);
        strTitle = iv.getStringExtra("Title");

        TextView txtHeader = (TextView) findViewById(R.id.txtHeader);
        txtHeader.setText(strTitle.toUpperCase());
        txtHeader.setTypeface(tf);

        arrVideoList = new ArrayList<VideoGeterSeter>();
        new getCategoryDetailImage().execute();

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();

            }
        });
        adVisibility = getResources().getString(R.string.ADS_VISIBILITY);
        interstitialAd = new InterstitialAd(this, getResources().getString(R.string.placement_id_fb_interstial));
        if (adVisibility.equals("YES")){
            interstitialAd.loadAd();
        } else {
            Log.i("Ads", "Ad not loaded");
        }
        // Set listeners for the Interstitial Ad
        interstitialAd.setAdListener(new InterstitialAdListener() {
            @Override
            public void onInterstitialDisplayed(Ad ad) {

            }

            @Override
            public void onInterstitialDismissed(Ad ad) {
                ContinueIntent();
            }

            @Override
            public void onError(Ad ad, AdError adError) {

                Log.i(LOG_TAG,"FAN ad error, ad can be shown from AdMob");

            }

            @Override
            public void onAdLoaded(Ad ad) {

                Log.i(LOG_TAG, "FAN interstitial ad loaded..");
            }

            @Override
            public void onAdClicked(Ad ad) {

            }

            @Override
            public void onLoggingImpression(Ad ad) {

            }
        });

        //this is for AdMob ads
        Log.i(LOG_TAG,"Activity loaded...");
        mInterstitialAd = new AdmobInterstitialAd(this);
        mInterstitialAd.setAdUnitId(getResources().getString(R.string.mediation_id));

        if (adVisibility.equals("YES")){
            mInterstitialAd.loadAd(new AdRequest.Builder().build());
        } else {
            Log.i("Ads", "Ad not loaded");
        }

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {

                Log.i(LOG_TAG, "Admob interstial ad loaded..");
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {

                Log.i(LOG_TAG, "Admob interstial ad failed to load.."+errorCode);
            }

            @Override
            public void onAdOpened() {
            }

            @Override
            public void onAdLeftApplication() {

            }

            @Override
            public void onAdClosed() {
                ContinueIntent();
            }
        });

    }

    public class getCategoryDetailImage extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(CategoryDeatilpage.this);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            getdetailforNearMe();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();

                Log.d("CateSize",""+arrVideoList.size());

                if (arrVideoList.size()==0){

                    relList.setVisibility(View.GONE);
                    relNodata.setVisibility(View.VISIBLE);

                } else {

                    relNodata.setVisibility(View.GONE);
                    relList.setVisibility(View.VISIBLE);

                }

                Categoriesgrid.setAdapter(new CategoriesOnlineGrid(CategoryDeatilpage.this, arrVideoList));

                Categoriesgrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position, long id) {
                        MAINPOSITION = position;
                        if (interstitialAd != null && interstitialAd.isAdLoaded()) {
                            interstitialAd.show();
                        } else {
                            if (mInterstitialAd !=null && mInterstitialAd.isLoaded()){
                                mInterstitialAd.show();
                                Log.i(LOG_TAG,"Ad showing from admob");
                            } else {
                                ContinueIntent();
                            }
                        }
                    }
                });

            }
        }
    }

    private void ContinueIntent() {

        Intent iv = new Intent(CategoryDeatilpage.this, Description.class);
        iv.putExtra("Id", arrVideoList.get(MAINPOSITION).getId());
        iv.putExtra("Position",""+MAINPOSITION);
        iv.putExtra("Code",""+arrVideoList.get(MAINPOSITION).getYtcode());
        iv.putExtra("Video", arrVideoList.get(MAINPOSITION).getVideo());
        iv.putExtra("Description", arrVideoList.get(MAINPOSITION).getDescription());
        iv.putExtra("Name", arrVideoList.get(MAINPOSITION).getName());
        iv.putExtra("ArraySize",""+arrVideoList.size());
        iv.putExtra("cat_id",""+arrVideoList.get(MAINPOSITION).getCat_id());

        startActivity(iv);
    }

    private void getdetailforNearMe() {
        URL hp = null;
        try {

            hp = new URL(getString(R.string.COMMON_URL)+getResources().getString(R.string.getVideoByidUrl)+strId);

            Log.d("hp",""+hp);

            URLConnection hpCon = hp.openConnection();
            hpCon.connect();
            InputStream input = hpCon.getInputStream();
            BufferedReader r = new BufferedReader(new InputStreamReader(input));
            String x = "";
            x = r.readLine();
            String total = "";

            while (x != null) {
                total += x;
                x = r.readLine();
            }
            JSONObject jObject = new JSONObject(total);
            Log.d("Object",""+jObject);

            JSONArray j = jObject.getJSONArray("List_Video");
            for (int i = 0; i < j.length(); i++)
            {
                JSONObject Obj;
                Obj = j.getJSONObject(i);

                Log.d("Object1",""+Obj);

                VideoGeterSeter temp = new VideoGeterSeter();

                temp.setId(Obj.getString("id"));
                temp.setVideo(Obj.getString("video"));
                temp.setYtcode(Obj.getString("ytcode"));
                temp.setName(Obj.getString("name"));
                temp.setDescription(Obj.getString("description"));
                temp.setCat_id(Obj.getString("cat_id"));
                Log.d("id",""+Obj.getString("id"));

                arrVideoList.add(temp);

            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
        }
    }

    @Override
    protected void onDestroy() {
        if (interstitialAd != null) {
            interstitialAd.destroy();
        }
        super.onDestroy();
    }

    public class CategoriesOnlineGrid extends BaseAdapter {

        private Activity activity;
        private ArrayList<VideoGeterSeter> data;
        private LayoutInflater inflater = null;

        public CategoriesOnlineGrid(Activity guide_Fragment, ArrayList<VideoGeterSeter> d) {
            activity = guide_Fragment;
            data = d;
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @SuppressLint("InflateParams")
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View vi = convertView;

            if (convertView == null) {
                vi = inflater.inflate(R.layout.grid_single, null);
            }

            TextView txtVideo = (TextView) vi.findViewById(R.id.txtVideo);
            txtVideo.setTypeface(tf);
            txtVideo.setText(data.get(position).getName());

            ImageView grid_image = (ImageView) vi.findViewById(R.id.grid_image);

            String strThumImage = data.get(position).getYtcode();
            Log.d("strThumImage",""+strThumImage);

            strThumImage = getResources().getString(R.string.thumbImageBaseUrl)+strThumImage+"/0.jpg";

            Glide.with(CategoryDeatilpage.this)
                    .load(strThumImage)
                    .placeholder(R.drawable.loading )
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(grid_image);

            return vi;
        }
    }

    /*@Override
    public void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        interstitialCanceled = false;
        if(getResources().getString(R.string.ADS_VISIBILITY).equals("YES")){
            CallNewInsertial();
        }else {
        }
    }*/

    /*private void CallNewInsertial() {
        cd = new ConnectionDetector(CategoryDeatilpage.this);

        if (!cd.isConnectingToInternet()) {
            *//*alert.showAlertDialog(getActivity(), "Internet Connection Error",
                    "Please connect to working Internet connection", false);*//*
            return;
        } else {
            mInterstitialAd = new InterstitialAd(CategoryDeatilpage.this);
            mInterstitialAd.setAdUnitId(getString(R.string.AdmobFullScreenAdsID));
            requestNewInterstitial();
            mInterstitialAd.setAdListener(new AdListener() {
                @Override
                public void onAdClosed() {
                    ContinueIntent();
                }
            });
        }
    }

    private void requestNewInterstitial() {
        AdRequest adRequest = new AdRequest.Builder().build();
        mInterstitialAd.loadAd(adRequest);
    }

    @Override
    public void onPause() {
        mInterstitialAd = null;
        interstitialCanceled = true;
        super.onPause();
    }*/
}
