package com.thelightapps.nagpurivideo;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Telephony;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.InterstitialAd;
import com.facebook.ads.InterstitialAdListener;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import in.srain.cube.views.GridViewWithHeaderAndFooter;
import io.realm.RealmResults;

public class FavouriteActivity extends AppCompatActivity {


    String LOG_TAG = this.getClass().getSimpleName();
    RelativeLayout relNodata;
    LinearLayout relList;
    //  GridView grid;
    GridViewWithHeaderAndFooter grid;
    ProgressDialog progressDialog;
    ArrayList<VideoGeterSeter> arrVideoList;
    ListView mDrawerList;
    String strTitle,myRegisterId;
    DrawerLayout mDrawerLayout;
    ActionBarDrawerToggle mDrawerToggle;
    String mActivityTitle,myTheme;
    SharedPreferences prefs;

    public static final int[] ArrayImage = {
            R.drawable.home, R.drawable.category,
            R.drawable.ic_fiber_new_black_24dp,R.drawable.ic_favorite_black_24dp,
            R.drawable.trending, R.drawable.like,
            R.drawable.share,
            R.drawable.upload, R.drawable.theme,
            R.drawable.app_share, R.drawable.more_app, R.drawable.rate};
    Typeface tf;
    int MAINPOSITION;
    String adVisibility;
    View layout12;
    boolean interstitialCanceled;
    private ConnectionDetector cd;
    private InterstitialAd interstitialAd;
    private AdmobInterstitialAd mInterstitialAd;


    RealmController realmController;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        tf = Typeface.createFromAsset(getApplicationContext().getAssets(),"fonts/Raleway-Light.ttf");

        prefs = getApplicationContext().getSharedPreferences("MY_PREFS_NAME", Context.MODE_PRIVATE);
        myTheme = prefs.getString("Color", null);
        myRegisterId = prefs.getString("RegisterId",null);
        Log.d("myRegisterIdM",""+myRegisterId);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_favoutrite);

        Intent iv = getIntent();
        strTitle = iv.getStringExtra("Type");

        // Instantiate an InterstitialAd object
        interstitialAd = new InterstitialAd(this, getResources().getString(R.string.placement_id_fb_interstial));

        if (myTheme!=null) {

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);

            android.support.v7.app.ActionBar actionBar = getSupportActionBar();
            actionBar.setDisplayShowHomeEnabled(false);
            actionBar.setDisplayOptions(actionBar.getDisplayOptions() | ActionBar.DISPLAY_SHOW_CUSTOM);

            TextView title = new TextView(actionBar.getThemedContext());
            title.setTextColor(Color.WHITE);
            title.setGravity(Gravity.RIGHT);

            if (strTitle.equals("Favourite")){
                title.setText(R.string.menuFavorite);
            }
            title.setTextSize(18.0f);
            title.setTypeface(tf);
            DisplayMetrics displaymetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
            int width = displaymetrics.widthPixels;
            title.setPadding(60, 0, 0, 0);
            title.setWidth(width);

            actionBar.setCustomView(title);
            actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor(myTheme)));

        } else {

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);

            android.support.v7.app.ActionBar actionBar = getSupportActionBar();
            actionBar.setDisplayShowHomeEnabled(false);
            actionBar.setDisplayOptions(actionBar.getDisplayOptions() | ActionBar.DISPLAY_SHOW_CUSTOM);

            TextView title = new TextView(actionBar.getThemedContext());
            title.setTextColor(Color.WHITE);
            title.setGravity(Gravity.RIGHT);

            if (strTitle.equals("Favourite")){
                title.setText("FAVOURITE");
            }
            title.setTypeface(tf);
            title.setTextSize(18.0f);
            DisplayMetrics displaymetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
            int width = displaymetrics.widthPixels;
            title.setPadding(60, 0, 0, 0);
            title.setWidth(width);

            actionBar.setCustomView(title);
            actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#f7412c")));

        }

        mDrawerList = (ListView) findViewById(R.id.navList);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mActivityTitle = getTitle().toString();

        relList = (LinearLayout) findViewById(R.id.relList);
        relNodata = (RelativeLayout) findViewById(R.id.relNodata);

        addDrawerItems();
        setupDrawer();

        realmController = new RealmController(getApplication());

        RealmResults<VideoGeterSeter> results = realmController.getVideos();
        arrVideoList = new ArrayList<VideoGeterSeter>(results);

        if (arrVideoList != null && arrVideoList.size() > 0) {
            grid = (GridViewWithHeaderAndFooter ) findViewById(R.id.grid);
            LayoutInflater layoutInflater = LayoutInflater.from(getApplicationContext());
            View headerView = layoutInflater.inflate(R.layout.test_header_view, null);

            grid.addHeaderView(headerView);

            grid.setAdapter(new FavouriteActivity.CustomGridOnline(FavouriteActivity.this, arrVideoList));

            grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {
                    MAINPOSITION = position;
                    if (interstitialAd != null && interstitialAd.isAdLoaded()) {
                        interstitialAd.show();
                    } else {
                        if (mInterstitialAd !=null && mInterstitialAd.isLoaded()){
                            mInterstitialAd.show();
                            Log.i(LOG_TAG,"Ad showing from admob");
                        } else {
                            ContinueIntent();
                        }
                    }

                }
            });
        }else {
            relList.setVisibility(View.GONE);
            relNodata.setVisibility(View.VISIBLE);
        }

        adVisibility = getResources().getString(R.string.ADS_VISIBILITY);

        if (adVisibility.equals("YES")){
            interstitialAd.loadAd();
        } else {
            Log.i("Ads", "Ad not loaded");
        }

        // Set listeners for the Interstitial Ad
        interstitialAd.setAdListener(new InterstitialAdListener() {
            @Override
            public void onInterstitialDisplayed(Ad ad) {

            }

            @Override
            public void onInterstitialDismissed(Ad ad) {
                ContinueIntent();
            }

            @Override
            public void onError(Ad ad, AdError adError) {


                Log.i(LOG_TAG,"FAN ad error, ad shown from AdMob");

            }

            @Override
            public void onAdLoaded(Ad ad) {
                Log.i(LOG_TAG, "FAN interstitial ad loaded..");

            }

            @Override
            public void onAdClicked(Ad ad) {

            }

            @Override
            public void onLoggingImpression(Ad ad) {

            }
        });

        //this is for AdMob ads
        Log.i(LOG_TAG,"Activity loaded...");
        mInterstitialAd = new AdmobInterstitialAd(this);
        mInterstitialAd.setAdUnitId(getResources().getString(R.string.mediation_id));
        if (adVisibility.equals("YES")){
            mInterstitialAd.loadAd(new AdRequest.Builder().build());
        } else {
            Log.i("Ads", "Ad not loaded");
        }

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {

                Log.i(LOG_TAG, "Admob interstial ad loaded..");
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {

                Log.i(LOG_TAG, "Admob interstial ad failed to load.."+errorCode);
            }

            @Override
            public void onAdOpened() {
            }

            @Override
            public void onAdLeftApplication() {

            }

            @Override
            public void onAdClosed() {
                ContinueIntent();
            }
        });
    }

    private void ContinueIntent() {

        Intent iv = new Intent(getApplicationContext(), Description.class);
        iv.putExtra("Id", arrVideoList.get(MAINPOSITION).getId());
        iv.putExtra("Position",""+MAINPOSITION);
        iv.putExtra("Code",""+arrVideoList.get(MAINPOSITION).getYtcode());
        iv.putExtra("Video", arrVideoList.get(MAINPOSITION).getVideo());
        iv.putExtra("Description", arrVideoList.get(MAINPOSITION).getDescription());
        iv.putExtra("Name", arrVideoList.get(MAINPOSITION).getName());
        iv.putExtra("ArraySize",""+arrVideoList.size());
        iv.putExtra("cat_id",arrVideoList.get(MAINPOSITION).getCat_id());

        startActivity(iv);
    }

    @Override
    protected void onDestroy() {
        if (interstitialAd != null) {
            interstitialAd.destroy();
        }
        super.onDestroy();
    }

    public class CustomGridOnline extends BaseAdapter {

        private Activity activity;
        private ArrayList<VideoGeterSeter> data;
        private LayoutInflater inflater = null;

        public CustomGridOnline(Activity guide_Fragment, ArrayList<VideoGeterSeter> d) {
            activity = guide_Fragment;
            data = d;
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @SuppressLint("InflateParams")
        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View vi = convertView;

            if (convertView == null) {
                vi = inflater.inflate(R.layout.favouriteadapter, null);
            }

            TextView txtVideo = (TextView) vi.findViewById(R.id.txtVideo);
            txtVideo.setTypeface(tf);
            txtVideo.setText(data.get(position).getName());

            ImageView grid_image = (ImageView) vi.findViewById(R.id.grid_image);

            ImageView deleteIV = (ImageView) vi.findViewById(R.id.delete_IV);
            deleteIV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    realmController.deleteById(data.get(position).getId());
                    try {
                        data.remove(position);
                        notifyDataSetChanged();
                    } catch (Exception e) {}
                }
            });
            String strThumImage = data.get(position).getYtcode();
            Log.d("strThumImage",""+strThumImage);

            strThumImage = getResources().getString(R.string.thumbImageBaseUrl)+strThumImage+"/0.jpg";

            Picasso.with(getApplicationContext())
                    .load(strThumImage)
                    .placeholder(R.drawable.defalt_img )
                    .into(grid_image);

            return vi;
        }
    }

    private void addDrawerItems() {

        String[] ArrayData = {getString(R.string.menuHome), getString(R.string.menuCategories),getString(R.string.menuLatestVideo),getString(R.string.menuFavorite),
                getString(R.string.menuTrending), getString(R.string.menuMostLiked), getString(R.string.menuMostShared),getString(R.string.menuAboutUs), getString(R.string.menuTheme),
                getString(R.string.menuShareApp), getString(R.string.menuMoreApp), getString(R.string.menuRateApp)};

        FavouriteActivity.LazyAdapter1 lazy1 = new FavouriteActivity.LazyAdapter1(FavouriteActivity.this, ArrayData, ArrayImage);
        mDrawerList.setAdapter(lazy1);

        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(MainActivity.this, "", Toast.LENGTH_SHORT).show();

                Log.d("position", "" + position);
                if (position == 0) {
                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(i);
                    overridePendingTransition(0, 0);
                    finish();
                } else if (position == 1) {
                    Intent i = new Intent(getApplicationContext(), Categories.class);
                    startActivity(i);
                    overridePendingTransition(0, 0);
                    finish();
                } else if (position == 2) {
                    Intent i = new Intent(getApplicationContext(), LatestVideoActivity.class);
                    i.putExtra("Type", "LatestVideo");
                    startActivity(i);
                    overridePendingTransition(0, 0);
                    finish();
                } else if (position == 3) {
                    mDrawerLayout.closeDrawers();
                    /*
                    Intent i = new Intent(getApplicationContext(), FavouriteActivity.class);
                    i.putExtra("Type","Favourite");
                    startActivity(i);
                    overridePendingTransition(0, 0);
                    finish();
                    */
                } else if (position == 4) {
                    Intent i = new Intent(getApplicationContext(), MostLike.class);
                    i.putExtra("Type","View");
                    startActivity(i);
                    overridePendingTransition(0, 0);
                    finish();
                } else if (position == 5) {
                    Intent i = new Intent(getApplicationContext(), MostLike.class);
                    i.putExtra("Type","Like");
                    startActivity(i);
                    overridePendingTransition(0, 0);
                    finish();
                } else if (position == 6) {
                    Intent i = new Intent(getApplicationContext(), MostLike.class);
                    i.putExtra("Type","Share");
                    startActivity(i);
                    overridePendingTransition(0, 0);
                    finish();
                } else if (position == 7){

                    Intent aboutIntent = new Intent(getApplicationContext(), aboutus.class);
                    startActivity(aboutIntent);

//                    Intent i = new Intent(MostLike.this,UploadVideo.class);
//                    startActivity(i);
//                    overridePendingTransition(0, 0);
//                    finish();

                } else if (position == 8){

                    Intent i = new Intent(getApplicationContext(),Setting.class);
                    startActivity(i);
                    overridePendingTransition(0, 0);
                    finish();

                } else if (position == 9){

                    mDrawerLayout.closeDrawers();

                    final RelativeLayout rl_dialoguser = (RelativeLayout) findViewById(R.id.rl_infodialog);
                    rl_dialoguser.setVisibility(View.VISIBLE);
                    layout12 = getLayoutInflater().inflate(R.layout.sharefriend, rl_dialoguser, false);

                    rl_dialoguser.addView(layout12);

                    Button btnFacebook = (Button) layout12.findViewById(R.id.btnFacebook);
                    Button btnEmail = (Button) layout12.findViewById(R.id.btnEmail);
                    Button btnMessage = (Button) layout12.findViewById(R.id.btnMessage);
                    Button btnTwitter = (Button) layout12.findViewById(R.id.btnTwitter);
                    Button btnWhatsapp = (Button) layout12.findViewById(R.id.btnWhatsapp);
                    Button btnExit = (Button) layout12.findViewById(R.id.btnExit);

                    btnFacebook.setTransformationMethod(null);
                    btnEmail.setTransformationMethod(null);
                    btnMessage.setTransformationMethod(null);
                    btnTwitter.setTransformationMethod(null);
                    btnWhatsapp.setTransformationMethod(null);
                    btnExit.setTransformationMethod(null);

                    GradientDrawable gdShare = new GradientDrawable();
                    GradientDrawable gdExit = new GradientDrawable();

                    if (myTheme!=null) {

                        gdShare.setShape(GradientDrawable.RECTANGLE);
                        gdShare.setColor(Color.parseColor(myTheme));
                        gdShare.setStroke(1, Color.WHITE);
                        gdShare.setCornerRadius(1.0f);

                        gdExit.setShape(GradientDrawable.RECTANGLE);
                        gdExit.setColor(Color.WHITE);
                        gdExit.setStroke(1, Color.parseColor(myTheme));
                        gdExit.setCornerRadius(5.0f);

                        btnExit.setTextColor(Color.parseColor(myTheme));

                    }else {

                        gdShare.setShape(GradientDrawable.RECTANGLE);
                        gdShare.setColor(Color.parseColor("#f7412c"));
                        gdShare.setStroke(1, Color.WHITE);
                        gdShare.setCornerRadius(1.0f);

                        gdExit.setShape(GradientDrawable.RECTANGLE);
                        gdExit.setColor(Color.WHITE);
                        gdExit.setStroke(1, Color.parseColor("#f7412c"));
                        gdExit.setCornerRadius(5.0f);

                    }

                    btnFacebook.setBackground(gdShare);
                    btnEmail.setBackground(gdShare);
                    btnMessage.setBackground(gdShare);
                    btnTwitter.setBackground(gdShare);
                    btnWhatsapp.setBackground(gdShare);

                    btnExit.setBackground(gdExit);

                    btnFacebook.setTypeface(tf);
                    btnEmail.setTypeface(tf);
                    btnMessage.setTypeface(tf);
                    btnTwitter.setTypeface(tf);
                    btnWhatsapp.setTypeface(tf);
                    btnExit.setTypeface(tf);

                    btnExit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            View myView = findViewById(R.id.rl_back);
                            ViewGroup parent = (ViewGroup) myView.getParent();
                            parent.removeView(myView);

                        }
                    });

                    btnFacebook.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            Intent intent = new Intent(Intent.ACTION_SEND);
                            intent.setType("text/plain");
                            intent.putExtra(Intent.EXTRA_TEXT, getString(R.string.SHARE_APP_LINK)+FavouriteActivity.this.getPackageName());
                            // See if official Facebook app is found
                            boolean facebookAppFound = false;
                            List<ResolveInfo> matches = getPackageManager().queryIntentActivities(intent, 0);
                            for (ResolveInfo info : matches) {
                                if (info.activityInfo.packageName.toLowerCase().startsWith("com.facebook")) {
                                    intent.setPackage(info.activityInfo.packageName);
                                    facebookAppFound = true;
                                    break;
                                }
                            }
                            // As fallback, launch sharer.php in a browser
                            if (!facebookAppFound) {
                                String sharerUrl = getResources().getString(R.string.facebookSharerUrl) + getString(R.string.SHARE_APP_LINK)+FavouriteActivity.this.getPackageName();
                                intent = new Intent(Intent.ACTION_VIEW, Uri.parse(sharerUrl));
                            }
                            startActivity(intent);

                            View myView = findViewById(R.id.rl_back);
                            ViewGroup parent = (ViewGroup) myView.getParent();
                            parent.removeView(myView);

                        }
                    });

                    btnEmail.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            try {

                                Intent gmail = new Intent(Intent.ACTION_VIEW);
                                gmail.setClassName("com.google.android.gm", "com.google.android.gm.ComposeActivityGmail");
                                gmail.putExtra(Intent.EXTRA_EMAIL, new String[] { "" });
                                gmail.setData(Uri.parse(""));
                                gmail.putExtra(Intent.EXTRA_SUBJECT, "Best App for Odia Video Songs:");
                                gmail.setType("text/plain");
                                gmail.setType("image/jpeg");
                                String pathofBmp = MediaStore.Images.Media.insertImage(getContentResolver(), BitmapFactory.decodeResource(getResources(),R.drawable.ic_launcher),"title", null);
                                Uri bmpUri = Uri.parse(pathofBmp);
                                gmail.putExtra(Intent.EXTRA_STREAM, bmpUri);
                                gmail.putExtra(Intent.EXTRA_TEXT, ""+ getString(R.string.SHARE_APP_LINK)+getApplicationContext().getPackageName());
                                startActivity(gmail);

                            } catch (Exception e) {

                                sendEmail();

                            }

                            //rl_dialoguser.setVisibility(View.GONE);
                            View myView = findViewById(R.id.rl_back);
                            ViewGroup parent = (ViewGroup) myView.getParent();
                            parent.removeView(myView);

                        }
                    });

                    btnTwitter.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            try {

                                ImageView imageView1 = new ImageView(getApplicationContext());
                                imageView1.setImageResource(R.drawable.ic_launcher);
                                imageView1.setVisibility(View.GONE);

                                Uri bmpUri = getLocalBitmapUri(imageView1);
//
                                Intent intent = new Intent();
                                intent.setAction(Intent.ACTION_SEND);
                                intent.putExtra(Intent.EXTRA_TEXT, getString(R.string.SHARE_APP_LINK)+getApplicationContext().getPackageName());
                                intent.putExtra(Intent.EXTRA_STREAM, bmpUri);
                                intent.setType("text/plain");
                                intent.setType("image/*");
                                intent.setPackage("com.twitter.android");
                                startActivity(intent);
                            } catch (ActivityNotFoundException e) {
//
                                Toast.makeText(getApplicationContext(), "Twitter not install", Toast.LENGTH_LONG).show();
                            }

                            //rl_dialoguser.setVisibility(View.GONE);
                            View myView = findViewById(R.id.rl_back);
                            ViewGroup parent = (ViewGroup) myView.getParent();
                            parent.removeView(myView);

                        }
                    });

                    btnMessage.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            //Share Image and Text

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) // At least KitKat
                            {
                                String defaultSmsPackageName = Telephony.Sms.getDefaultSmsPackage(getApplication()); // Need to change the build to API 19

                                Intent sendIntent = new Intent(Intent.ACTION_SEND);
                                sendIntent.setType("vnd.android-dir/mms-sms");
                                sendIntent.putExtra("sms_body", getString(R.string.SHARE_APP_LINK));
                                sendIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(MediaStore.Images.Media.insertImage(getContentResolver(), BitmapFactory.decodeResource(getResources(),R.drawable.ic_launcher),"title", null)));
                                sendIntent.setType("image/png");

                                if (defaultSmsPackageName != null)// Can be null in case that there is no default, then the user would be able to choose
                                // any app that support this intent.
                                {
                                    sendIntent.setPackage(defaultSmsPackageName);
                                }
                                startActivity(sendIntent);

                            }
                            else
                            {
                                Intent smsIntent = new Intent(android.content.Intent.ACTION_VIEW);
                                smsIntent.setType("vnd.android-dir/mms-sms");
                                smsIntent.putExtra("sms_body", getString(R.string.SHARE_APP_LINK));
                                smsIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(MediaStore.Images.Media.insertImage(getContentResolver(), BitmapFactory.decodeResource(getResources(),R.drawable.ic_launcher),"title", null)));
                                smsIntent.setType("image/png");
                                startActivity(smsIntent);
                            }

                            View myView = findViewById(R.id.rl_back);
                            ViewGroup parent = (ViewGroup) myView.getParent();
                            parent.removeView(myView);

                        }
                    });

                    btnWhatsapp.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            try {
                                String urlToShare ="Best App for Odia Video Songs::"+"\n"+getString(R.string.SHARE_APP_LINK)+getApplicationContext().getPackageName()+"\n";

                                Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
                                whatsappIntent.putExtra(Intent.EXTRA_TEXT, urlToShare);
                                whatsappIntent.setType("text/plain");
                                whatsappIntent.setPackage("com.whatsapp");
                                startActivity(whatsappIntent);
                                // Detailpage.this.startActivity(whatsappIntent);

                            } catch (ActivityNotFoundException ex) {
                                Toast.makeText(getApplicationContext(), "Whatsapp have not been installed.", Toast.LENGTH_LONG)
                                        .show();
                            }

                            View myView = findViewById(R.id.rl_back);
                            ViewGroup parent = (ViewGroup) myView.getParent();
                            parent.removeView(myView);

                        }
                    });

                } else if (position == 10){
                    Uri uri1 = Uri.parse(getString(R.string.MORE_APP_LINK));
                    Intent iv1 = new Intent(Intent.ACTION_VIEW, uri1);
                    startActivity(iv1);

                } else if (position == 11){
                    Uri uri = Uri.parse(getString(R.string.RATE_APP_LINK)+ getApplicationContext().getPackageName());
                    Intent iv = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(iv);
                }
            }
        });
    }

    private void sendEmail() {
        // TODO Auto-generated method stub
        String recipient = "";
        String subject = "WALLPAPER TEMPLATE";
        @SuppressWarnings("unused")
        String body = "";

        String[] recipients = { recipient };
        Intent email = new Intent(Intent.ACTION_SEND);

        email.setType("message/rfc822");

        email.putExtra(Intent.EXTRA_EMAIL, getString(R.string.SHARE_APP_LINK)+getApplicationContext().getPackageName());
        email.putExtra(Intent.EXTRA_SUBJECT, subject);

        try {

            startActivity(Intent.createChooser(email, ""));

        } catch (android.content.ActivityNotFoundException ex) {

            //Toast.makeText(Setting.this, getString(R.string.email_no_client), Toast.LENGTH_LONG).show();

        }
    }

    public Uri getLocalBitmapUri(ImageView imageView) {
        // Extract Bitmap from ImageView drawable
        Drawable drawable = imageView.getDrawable();
        Bitmap bmp = null;
        if (drawable instanceof BitmapDrawable) {
            bmp = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
        } else {
            return null;
        }
        // Store image to default external storage directory
        Uri bmpUri = null;
        try {

            File file = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES),
                    "share_image_" + System.currentTimeMillis() + ".png");
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            bmpUri = Uri.fromFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }

    private void setupDrawer() {
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close) {

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                //getSupportActionBar().setTitle("Navigation!");
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                getSupportActionBar().setTitle(mActivityTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        // Activate the navigation drawer toggle
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        /*AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to exit?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int id) {
                        MostLike.this.finishAffinity();

                        SharedPreferences preferences = getSharedPreferences("MY_PREFS_NAME", 0);
                        preferences.edit().remove("CurrentPosition").commit();

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();*/
        MainActivity.showDialog(this);
        //	super.onBackPressed();

    }

    public class LazyAdapter1 extends BaseAdapter {

        Activity activity;
        String[] data;
        int[] Image;
        LayoutInflater inflater = null;

        public LazyAdapter1(Activity guide_Fragment, String[] d, int[] image) {
            activity = guide_Fragment;
            data = d;
            Image = image;
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return data.length;
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @SuppressLint("InflateParams")
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View vi = convertView;

            if (convertView == null) {
                vi = inflater.inflate(R.layout.drawercell, null);
            }

            ColorFilter cf;

            RelativeLayout relMidleLine = (RelativeLayout) vi.findViewById(R.id.relMidleLine);

            TextView topic_name = (TextView) vi.findViewById(R.id.txtDrawer);
            topic_name.setText(data[position]);
            topic_name.setTypeface(tf);

            ImageView imageView = (ImageView) vi.findViewById(R.id.imageView);
            //imageView.setImageResource(Image[position]);

            Bitmap bmp = BitmapFactory.decodeResource(getResources(), Image[position]);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.JPEG, 50, stream);

            imageView.setImageBitmap(bmp);

            if (myTheme!=null) {

                cf = new PorterDuffColorFilter(Color.parseColor(myTheme), PorterDuff.Mode.SRC_IN);
                imageView.setColorFilter(cf);

                relMidleLine.setBackgroundColor(Color.parseColor(myTheme));

            }else {
                cf = new PorterDuffColorFilter(Color.parseColor("#f7412c"), PorterDuff.Mode.SRC_IN);
                imageView.setColorFilter(cf);
                relMidleLine.setBackgroundColor(Color.parseColor("#f7412c"));
            }

            return vi;
        }
    }

    @Override
    public void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        interstitialCanceled = false;
    }

    @Override
    public void onPause() {
        interstitialCanceled = true;
        super.onPause();
    }
}
